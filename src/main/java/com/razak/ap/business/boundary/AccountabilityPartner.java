package com.razak.ap.business.boundary;

import com.razak.ap.business.control.ApController;
import com.razak.ap.business.control.UserRepository;
import com.razak.ap.business.entity.User;

import java.util.List;

public class AccountabilityPartner {

	private ApController controller;
	private UserRepository userRepository;

	public AccountabilityPartner(ApController controller, UserRepository userRepository) {
		this.controller = controller;
		this.userRepository = userRepository;
	}

	public List<User> getUsers() {
		return userRepository.findAll();
	}

	public void addUser(User user) {
		userRepository.add(user);
	}

	public List<User> findPartner() {
		List<User> all = userRepository.findAll();
		return controller.find(all);
	}
}
