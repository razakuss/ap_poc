package com.razak.ap.business.entity;

import java.util.List;

public class User {
	private String name;
	private String email;
	private List<Theme> themes;

	public User(String name, String email, List<Theme> themes) {
		this.name = name;
		this.email = email;
		this.themes = themes;
	}

	public User(int index, List<Theme> themes) {
		this.name = "User" + Integer.toString(index);
		this.email = emailForUser(this.name);
		this.themes = themes;
	}

	private static String emailForUser(String userName) {
		return userName + "@gmail.com";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Theme> getThemes() {
		return themes;
	}

	public void setThemes(List<Theme> themes) {
		this.themes = themes;
	}
}
