package com.razak.ap.business.entity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum Theme {

	Achiever("Achiever", "Osiąganie"),
	Activator("Activator", "Aktywator"),
	Adaptability("Adaptability","Elastyczność"),
	Analytical("Analytical","Analityk"),
	Arranger("Arranger", "Organizator"),
	Belief("Belief", "Pryncypialność"),
	Command("Command","Dowodzenie"),
	Communication("Communication","Komunikatywność"),
	Competition("Competition","Rywalizacja"),
	Connectedness("Connectedness","Współzależność"),
	Consistency("Consistency","Bezstronność"),
	Context("Context","Kontekst"),
	Deliberative("Deliberative", "Rozwaga"),
	Developer("Developer", "Rozwijanie innych"),
	Discipline("", "Dyscyplina"),
	Empathy("", "Empatia"),
	Focus("Focus","Ukierunkowanie"),
	Futuristic("Futuristic","Wizjoner"),
	Harmony("Harmony","Zgodność"),
	Ideation("Ideation","Odkrywczość"),
	Includer("Includer","Integrator"),
	Individualization("Individualization","Indywidualizacja"),
	Input("Input","Zbieranie"),
	Intellection("Intellection","Intelekt"),
	Learner("Learner","Uczenie się"),
	Maximizer("Maximizer","Maksymalista"),
	Positivity("Positivity","Optymista"),
	Relator("Relator","Bliskość"),
	Responsibility("Responsibility","Odpowiedzialność"),
	Restorative("Restorative","Naprawianie"),
	SelfAssurance("Self-assurance","Wiara w siebie"),
	Significance("Significance","Poważanie"),
	Strategic("Strategic","Strateg"),
	Woo("Woo","Czar");

	private final String name;
	private final String translate;
	private static final List<Theme> VALUES =
			Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	Theme(String name, String translate) {
		this.name = name;
		this.translate = translate;
	}

	public static Theme randomTheme()  {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}

	public static List<Theme> getValues() {
		return VALUES;
	}

}
