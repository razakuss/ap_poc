package com.razak.ap.business.control;

import com.razak.ap.business.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ApController {

	private static final Random RANDOM = new Random();

	public List<User> find(List<User> users) {
		List<User> chosenUsers = new ArrayList<>();
		if (!users.isEmpty()) {
			int index = RANDOM.nextInt(users.size());
			chosenUsers.add(users.get(index));
		}
		else {
			chosenUsers.add(new User("unknown", "unknown", new ArrayList<>()));
		}
		return chosenUsers;
	}
}
