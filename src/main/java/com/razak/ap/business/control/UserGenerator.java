package com.razak.ap.business.control;

import com.razak.ap.business.entity.Theme;
import com.razak.ap.business.entity.User;

import java.awt.font.NumericShaper;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class UserGenerator {

	public static List<User> generate(int amount) {
		List<User> users = new ArrayList<>();
		for (int i = 0; i < amount; i++) {
			users.add(createUser(i));
		}
		return users;
	}

	private static User createUser(int index) {
		return new User(index, getThemes());
	}

	private static ArrayList<Theme> getThemes() {
		ArrayList<Theme> themes = new ArrayList<>();
		themes.add(Theme.randomTheme());
		themes.add(Theme.randomTheme());
		themes.add(Theme.randomTheme());
		themes.add(Theme.randomTheme());
		themes.add(Theme.randomTheme());
		return themes;
	}
}
