package com.razak.ap.business.control;

import com.razak.ap.business.entity.User;

import java.util.List;

public class UserRepository implements Repository<User> {

	private List<User> users;

	public UserRepository(List<User> users) {
		this.users = users;
	}

	public List<User> findAll() {
		return users;
	}

	public void add(User user) {
		users.add(user);
	}
}
