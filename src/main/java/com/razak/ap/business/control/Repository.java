package com.razak.ap.business.control;

import java.util.List;

public interface Repository<T> {

	List<T> findAll();

	void add(T t);
}
