package com.razak.ap.presentation.boundary;

import com.razak.ap.business.boundary.AccountabilityPartner;
import com.razak.ap.business.control.ApController;
import com.razak.ap.business.control.UserGenerator;
import com.razak.ap.business.control.UserRepository;
import com.razak.ap.business.entity.User;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Named
@ViewScoped
public class ApView implements Serializable {

	AccountabilityPartner accountabilityPartner;
	private String firstTheme = "";
	private String secondTheme = "";
	private String thirdTheme = "";
	private String fourthTheme = "";
	private String fifthTheme = "";
	private List<User> partners = new ArrayList<>();

	@PostConstruct
	public void init() {
		List<User> users = UserGenerator.generate(20);
		accountabilityPartner = new AccountabilityPartner(new ApController(), new UserRepository(users));
	}

	public void buttonAction(ActionEvent actionEvent) {
		if (areNotThemeSelected()) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Select at least one theme"));
		}
		else {
			partners = accountabilityPartner.findPartner();
		}
	}

	private boolean areNotThemeSelected() {
		return
				isThemeNotSelected(firstTheme) &&
				isThemeNotSelected(secondTheme) &&
				isThemeNotSelected(thirdTheme) &&
				isThemeNotSelected(fourthTheme) &&
				isThemeNotSelected(fifthTheme);
	}

	private boolean isThemeNotSelected(String selectedTheme) {
		return Objects.isNull(selectedTheme) || selectedTheme.isEmpty();
	}

	public void clearSelectedThemes(final AjaxBehaviorEvent event) {
		partners.clear();
	}

	public String getFirstTheme() {
		return firstTheme;
	}

	public void setFirstTheme(String firstTheme) {
		this.firstTheme = firstTheme;
	}

	public String getSecondTheme() {
		return secondTheme;
	}

	public void setSecondTheme(String secondTheme) {
		this.secondTheme = secondTheme;
	}

	public String getThirdTheme() {
		return thirdTheme;
	}

	public void setThirdTheme(String thirdTheme) {
		this.thirdTheme = thirdTheme;
	}

	public String getFourthTheme() {
		return fourthTheme;
	}

	public void setFourthTheme(String fourthTheme) {
		this.fourthTheme = fourthTheme;
	}

	public String getFifthTheme() {
		return fifthTheme;
	}

	public void setFifthTheme(String fifthTheme) {
		this.fifthTheme = fifthTheme;
	}

	public List<User> getPartners() {
		return partners;
	}
}
