package com.razak.ap.presentation.boundary;

import com.razak.ap.business.entity.Theme;

import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class ThemeView {
	public List<String>getThemes() {
		List<String> themes =
				Theme.getValues()
				.stream()
				.map(t -> t.name())
				.collect(Collectors.toList());
		return themes;
	}
}
